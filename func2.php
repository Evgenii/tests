<?php
// for PHP >= 5.4.0

$greet = function($name1, $name2)
{
    printf("Hello, this %s||%s||%s\r\n", $this->name, $name1, $name2);
};

class A
{
	public $name;
	private $func1;
	private $func2;

	public function __construct()
	{
		global $greet;

		$this->name = "classA";
		$this->func1 = function($name1, $name2)
		{
			printf("Hello %s||%s||%s\r\n", $this->name, $name1, $name2);
		};
		$this->func2 = $greet;
	}

	public function test1($func)
	{
		$func('World', 'PHP');
	}

	public function test2($str1, $str2)
	{
		$func = $this->func1;
		$func($str1, $str2);
		$this->test1($func);
//		$this->test1($this->func2);
	}
}

$a = new A();

//$a->test1($greet);
$a->test2('World2', 'PHP2');

?>
