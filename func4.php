<?php
// for PHP >= 5.4.0

class A
{
	public $name;
	private $func;
	private $owner;

	public function __construct()
	{
		$this->name = "classA";
		$this->func = function($name1, $name2)
			{
				printf("Hello %s||%s||%s\r\n", $this->name, $name1, $name2);
			};
	}

	public function getCallback($owner)
	{
		$func = function($s1, $s2) use ($owner)
			{
				list ($name1, $name2) = $owner->getName();
				$this->test2($s2, $name2);
			};
		return $func;
	}

	public function test1($func)
	{
		$func('World', 'PHP');
	}

	public function test2($str1, $str2)
	{
		$func = $this->func;
		$func($str1, $str2);
		$this->test1($func);
	}
}

class B
{
	protected $name1;
	protected $name2;
	private $func;

	public function __construct($a, $str1, $str2)
	{
		$this->name1 = $str1;
		$this->name2 = $str2;
		$this->func = $a->getCallback($this);
	}

	public function test()
	{
		$func = $this->func;
		$func($this->name1, $this->name2);
	}

	public function getName()
	{
		return array($this->name1, $this->name2);
	}
}

$a = new A();

$b1 = new B($a, 'World3', 'PHP3');
$b2 = new B($a, 'World4', 'PHP4');

$b2->test();
$b1->test();

?>
