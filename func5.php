<?php
// for PHP >= 5.4.0

class A
{
	public $name;
	private $func;

	public function __construct()
	{
		$this->name = "classA";
		$this->func = function($name1, $name2)
			{
				printf("Hello %s||%s||%s\r\n", $this->name, $name1, $name2);
			};
	}

	public function getCallback($owner)
	{
		$func = function($s1, $s2) use ($owner)
			{
				$this->test2($s1, get_class($owner));
			};
		return $func;
	}

	public function test1($func)
	{
		$func('World', 'PHP');
	}

	public function test2($str1, $str2)
	{
		$func = $this->func;
		$func($str1, $str2);
//		$this->test1($func);
	}
}

class B
{
	public $name;
	protected $func;

	public function __construct($a, $str)
	{
		$this->name = $str;
		$this->func = $a->getCallback($this);
	}

	public function test()
	{
		$func = $this->func;
		$func($this->name, 'non-visible');
	}
}

class C extends B
{
}

class D extends B
{
}

$a = new A();

$c = new C($a, 'OBJ #1');
$d = new D($a, 'OBJ #2');

$d->test();
$c->test();

?>
