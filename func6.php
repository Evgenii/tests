<?php
// for PHP >= 5.4.0

class A
{
	public $name;
	private $func;

	public function __construct()
	{
		$this->name = "classA";
		$this->func = function($name1, $name2)
			{
				printf("Hello %s||%s||%s\r\n", $this->name, $name1, $name2);
			};
	}

	public function getCallback($owner)
	{
		$func1 = function($s1, $s2) use ($owner)
			{
				$this->test2($s1, get_class($owner));
			};
		$func2 = function($s1, $s2) use ($owner)
			{
				return array("$s1|$s2",  get_class($owner));
			};
		return array($func1, $func2);
	}

	public function test1($func)
	{
		$func('World', 'PHP');
	}

	public function test2($str1, $str2)
	{
		$func = $this->func;
		$func($str1, $str2);
//		$this->test1($func);
	}

	public function test3($args)
	{
		call_user_func_array($this->func, $args);
	}
}

class B
{
	public $name;
	protected $func1;
	protected $func2;
	private $a;

	public function __construct($a, $str)
	{
		$this->name = $str;
		$this->a = $a;
		list ($this->func1, $this->func2) = $a->getCallback($this);
	}

	public function test()
	{
		$func1 = $this->func1;
		$func1($this->name, 'non-visible');
		$func2 = $this->func2;
		$this->a->test3($func2('=='.$this->name.'==', 'str2'));
	}
}

class C extends B
{
}

class D extends B
{
}

$a = new A();

$c = new C($a, 'OBJ #1');
$d = new D($a, 'OBJ #2');

$d->test();
$c->test();

?>
