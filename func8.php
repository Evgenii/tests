<?php
// for special PHP >= 5.3.0

class A
{
	public $name;

	public function __construct()
	{
		$this->name = "classA";
	}

	public function getCallback($owner)
	{
		$my = $this;
		$func1 = function($s1, $s2) use ($owner, $my)
			{
				$my->test1($s1, get_class($owner));
			};
		$func2 = function($s1, $s2) use ($owner)
			{
				return array("$s1|$s2",  get_class($owner));
			};
		return array($func1, $func2);
	}

	public function test1($name1, $name2)
	{
		printf("Hello %s||%s||%s\r\n", $this->name, $name1, $name2);
	}

	public function test2($args)
	{
		return call_user_func_array(array($this, 'test1'), $args);
	}
}

class B
{
	public $name;
	protected $func1 = null;
	protected $func2;
	private $a;

	public function __construct($a, $str)
	{
		$this->name = $str;
		$this->a = $a;
		list ($this->func1, $this->func2) = $a->getCallback($this);
	}
	
	public function __call($name, $arguments)
	{
		if (isset($this->$name) && is_callable($this->$name)) {
			$func = $this->$name;
			return call_user_func_array($func, $arguments);
		}
		return null;
	}

	public function test()
	{
		$func1 = $this->func1;
		$func1($this->name, 'non-visible');
		$func2 = $this->func2;
		$this->a->test2($func2('=='.$this->name.'==', 'str2'));
	}
}

class C extends B
{
}

class D extends B
{
}

$a = new A();

$c = new C($a, 'OBJ #1');
$d = new D($a, 'OBJ #2');

//$d->test();
$d->func1($d->name, 'non-visible');
var_dump($d->func2('=='.$d->name.'==', 'str2'));
var_dump($d->func3($d->name, 'test func3'));
$c->test();

?>
