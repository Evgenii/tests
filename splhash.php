
<?php
class A {
    public $id = 1;

    public function __construct($id) {
        echo "A!\n";
	$this->id = $id;
    }
}

class B extends A {
    public $id = 2;

    public function __construct() {
        echo "B!\n";
    }
}

$a1 = new A(4);
echo spl_object_hash($a1) . "\n";
$a1->id = 3;
echo spl_object_hash($a1) . "\n";
$a2 = new A(4);
echo spl_object_hash($a2) . "\n";
?>
