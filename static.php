
<?php
class AB {
    public function __construct() {
        echo "AB!\n";
    }
}

class A extends AB {
    public function __construct() {
        parent::__construct();
	$this->Init();
    }
    public function Init() {
//die(get_called_class());
        $this->bla();
        static::bla();
    }
    private function foo() {
        echo "success!\n";
    }
    public function test() {
//        $this->bla();
//        static::bla();
        $this->foo();
        static::foo();
    }
    public function bla() {
        echo "A!\n";
    }
}

class B extends A {
   /* foo() ����� ���������� � �, ������������� ��� ������� �������� �� �������� �,
      � ����� ����� �������*/
    public function bla() {
        echo "B!\n";
    }
}

class C extends A {
    private function foo() {
        /* �������� ����� �������; ������� �������� ������ ������ � */
    }
    public function bla() {
        echo "C!\n";
    }
}

$b = new B();
$b->test();
$c = new C();
$c->test();   //�� �����
?>
