<?php

class Base_Store
{
	protected static $_instance;
	public $test;
	
	public function __construct()
	{
		$this->test = 1;
	}
	
	public static function getInstance()
	{
		if (!self::$_instance) {
			self::$_instance = new static();
		}
		return self::$_instance;
	}
	
	public static function getInstance2()
	{
		if (!static::$_instance) {
			static::$_instance = new static();
		}
		return static::$_instance;
	}
}

class Store2 extends Base_Store
{
	public function __construct()
	{
		$this->test = 2;
	}
}

class Store3 extends Base_Store
{
	public function __construct()
	{
		$this->test = 3;
	}
	
	public static function getInstance3()
	{
		if (!self::$_instance) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}
}

class Store4 extends Store3
{
	public function __construct()
	{
		$this->test = 4;
	}
}

$o1 = Store2::getInstance();
echo $o1->test."\n";

$o1->test = 5;

$o2 = Store4::getInstance3();
echo $o1->test."\n";

?>